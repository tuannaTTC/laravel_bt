@extends('layouts.backend')

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/plugins/simplemde/simplemde.min.js') }}"></script>
    <script src="{{ asset('js/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/oneui.core.min.js') }}"></script>

    <script>jQuery(function () {
            One.helpers(['summernote', 'ckeditor', 'simplemde']);
        });</script>

@endsection
@section('content')

    <div class="content">
        <form class="form-group" action="{{route('newsAdd.post')}}" method="POST">
            @csrf
            <div class="form-group">
                <h3 class="block-title">Title</h3>
                <input class="form-control" type="text" name="title">
            </div>
            <div class="form-group">
                <h3 class="block-title">Slug</h3>
                <input class="form-control" type="text" name="slug">
            </div>
            <div class="form-group">
                <h3 class="block-title">Category</h3>
                <select class="form-control selected" name="category_id" id="">
                    @foreach($listCate as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <!-- Summernote (.js-summernote + .js-summernote-air classes are initialized in Helpers.summernote()) -->
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">Content</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <textarea id="js-ckeditor" name="news_content"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-6 text-right">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-check mr-1"></i> Submit
                </button>
            </div>

        </form>
    </div>



@endsection
