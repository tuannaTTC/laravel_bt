@extends('layouts.backend')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">Dynamic Table <small>Full</small></h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center" style="width: 80px;">id</th>
                    <th>Title</th>
                    <th  class="d-none d-sm-table-cell" style="width: 30%;">Slug</th>
                    <th>Content</th>
                    <th style="width: 15%;">Category Id</th>
                    <th style="width: 15%;">Create By</th>
                    <th style="width: 15%;">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($all as $item)
                    <tr>
                        <td class="text-center">{{ $item->id }}</td>
                        <td class="font-w600">{{ $item->title }}</td>
                        <td class="font-w600">{{ $item->slug }}</td>
                        <td class="d-none d-sm-table-cell">{{ $item->content }}</td>
                        <td>{{ $item->cate_id }}</td>
                        <td>{{ $item->created_by }}</td>
                        <td >
                            <a href="dashboard/add" style="display: inline-block; margin: 0 5px" data-toggle="tooltip" data-placement="top" title="Add">
                                <i class="fas fa-plus-square"></i>
                            </a>
                            <a href="{{route('newsUpdate.get',['id' => $item->id])}}" style="display: inline-block; margin: 0 5px"  data-toggle="tooltip" data-placement="top" title="Update">
                                <i class="fas fa-list-alt"></i>
                            </a>
                            <a href="{{route('newsDelete.delete',['id' => $item->id])}}" style="display: inline-block; margin: 0 5px"  data-toggle="tooltip" data-placement="top" title="Delete">
                                <i class="fas fa-trash-alt"></i>
                            </a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

