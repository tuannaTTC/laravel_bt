@extends('layouts.simple')

@section('content')
    <div id="page-container"
         class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed main-content-narrow"
         style="padding-left: 0">
        <!-- Main Container -->
        <main id="main-container">

            <!-- Hero Content -->
            <div class="bg-image" style="background-image: url('assets/media/photos/photo18@2x.jpg');">
                <div class="bg-primary-dark-op">
                    <div class="content content-full overflow-hidden">
                        <div class="mt-7 mb-5 text-center">
                            <h1 class="h2 text-white mb-2 invisible" data-toggle="appear"
                                data-class="animated fadeInDown">The latest stories only for you.</h1>
                            <h2 class="h4 font-w400 text-white-75 invisible" data-toggle="appear"
                                data-class="animated fadeInDown">Feel free to explore and read.</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Hero Content -->

            <!-- Page Content -->
            <div class="content content-side-full">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                    @foreach($allNews as $item)
                        <!-- Story -->
                            <div class="push invisible" data-toggle="appear" data-offset="50"
                                 data-class="animated fadeIn">
                                <a class="block block-rounded block-link-pop" href="be_pages_blog_story.html">
                                    <img class="img-fluid" src="assets/media/photos/photo22@2x.jpg" alt="">
                                    <div class="block-content">
                                        <h4 class="mb-1">{{$item->title}}</h4>
                                        @action('isTinHost',$item)
                                        <p class="font-size-sm">
                                            <span class="text-primary">{{$item->slug}}</span> {{$item->created_at}}
                                        </p>
                                        <p class="font-size-sm">
                                            {!! $item->content !!}
                                        </p>
                                    </div>
                                </a>
                            </div>
                            <!-- END Story -->
                    @endforeach

                    <!-- Pagination -->
                        <nav aria-label="Page navigation">
                            {{$allNews->links('custom')}}
                        </nav>
                        <!-- END Pagination -->
                    </div>
                </div>
            </div>
            <!-- END Page Content -->

            <!-- Get Started -->
            <div class="bg-body-dark">
                <div class="content content-full">
                    <div class="my-5 text-center">
                        <h3 class="h4 mb-4 invisible" data-toggle="appear">Do you like our stories? Sign up today and
                            get access to over <strong>10.000</strong> travel stories!</h3>
                        <a class="btn btn-rounded btn-primary px-4 py-2 invisible" data-toggle="appear"
                           data-class="animated bounceIn" href="javascript:void(0)">Get Started Today</a>
                    </div>
                </div>
            </div>
            <!-- END Get Started -->
        </main>
        <!-- END Main Container -->


        <!-- Apps Modal -->
        <!-- Opens from the modal toggle button in the header -->
        <div class="modal fade" id="one-modal-apps" tabindex="-1" role="dialog" aria-labelledby="one-modal-apps"
             aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="block block-rounded block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Apps</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row gutters-tiny">
                                <div class="col-6">
                                    <!-- CRM -->
                                    <a class="block block-rounded block-link-shadow bg-body" href="javascript:void(0)">
                                        <div class="block-content text-center">
                                            <i class="si si-speedometer fa-2x text-primary"></i>
                                            <p class="font-w600 font-size-sm mt-2 mb-3">
                                                CRM
                                            </p>
                                        </div>
                                    </a>
                                    <!-- END CRM -->
                                </div>
                                <div class="col-6">
                                    <!-- Products -->
                                    <a class="block block-rounded block-link-shadow bg-body" href="javascript:void(0)">
                                        <div class="block-content text-center">
                                            <i class="si si-rocket fa-2x text-primary"></i>
                                            <p class="font-w600 font-size-sm mt-2 mb-3">
                                                Products
                                            </p>
                                        </div>
                                    </a>
                                    <!-- END Products -->
                                </div>
                                <div class="col-6">
                                    <!-- Sales -->
                                    <a class="block block-rounded block-link-shadow bg-body mb-0"
                                       href="javascript:void(0)">
                                        <div class="block-content text-center">
                                            <i class="si si-plane fa-2x text-primary"></i>
                                            <p class="font-w600 font-size-sm mt-2 mb-3">
                                                Sales
                                            </p>
                                        </div>
                                    </a>
                                    <!-- END Sales -->
                                </div>
                                <div class="col-6">
                                    <!-- Payments -->
                                    <a class="block block-rounded block-link-shadow bg-body mb-0"
                                       href="javascript:void(0)">
                                        <div class="block-content text-center">
                                            <i class="si si-wallet fa-2x text-primary"></i>
                                            <p class="font-w600 font-size-sm mt-2 mb-3">
                                                Payments
                                            </p>
                                        </div>
                                    </a>
                                    <!-- END Payments -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Apps Modal -->
    </div>

@endsection
