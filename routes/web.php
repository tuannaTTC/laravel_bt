<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Example Routes
Route::view('/', 'landing');
//Route::match(['get', 'post'], '/dashboard', function(){
//    return view('dashboard');
//});
Route::view('/pages/slick', 'pages.slick');
Route::view('/pages/datatables', 'pages.datatables');
Route::view('/pages/blank', 'pages.blank');

Route::prefix('dashboard')->group(function () {

    Route::get('/', [
        'as' => 'dashboard.index',
        'uses' => 'NewsController@index'
    ]);
    Route::get('/add', [
        'as' => 'newsAdd.get',
        'uses' => 'NewsController@addGet'
    ]);
    Route::post('/add', [
        'as' => 'newsAdd.post',
        'uses' => 'NewsController@addPost'
    ]);
    Route::get('/delete/{id}', [
        'as' => 'newsDelete.delete',
        'uses' => 'NewsController@delete'
    ]);
    Route::get('/update/{id}', [
        'as' => 'newsUpdate.get',
        'uses' => 'NewsController@updateGet'
    ]);
    Route::post('/update', [
        'as' => 'newsUpdate.put',
        'uses' => 'NewsController@updatePut'
    ]);

});

Route::get('/home', [
    'as' => '',
    'uses' => 'NewsController@showAll'
]);
