<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\news;
use Illuminate\Http\Request;
use TorMorten\Eventy\Facades\Events as Eventy;

class UserController extends Controller
{
    public function Test(){
        Eventy::addAction('my.hook', function($user) {
            if ($user->is_awesome) {
                $this->doSomethingAwesome($user);
            }
        });
    }
}
