<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\news;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TorMorten\Eventy\Facades\Events as Eventy;

class NewsController extends Controller
{

    private $news;
    private $category;

    public function __construct(news $news,category $category){
        $this->news = $news;
        $this->category = $category;
    }
    //
    public function index()
    {
        $all = $this->news->get();
        return view('dashboard', compact('all'));
    }

    public function addGet()
    {
        $listCate = $this->category->get();
        return view('add', compact('listCate'));
    }

    public function addPost(Request $request)
    {
        $this->news->insert([
            'title' => $request->title,
            'content' => $request->news_content,
            'created_by' => 'ATN',
            'cate_id' => $request->category_id,
            'slug' => $request->slug,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return redirect()->route('dashboard.index');
    }

    public function delete($id)
    {
        $this->news->where('id', '=', $id)->delete();
        return redirect()->route('dashboard.index');
    }

    public function updateGet($id)
    {
        $news = $this->news->find($id);
        $listCate = DB::table('categories')->get();
        return view('update', compact('news'), compact('listCate'));
    }

    public function updatePut(Request $request)
    {
        $news = $this->news->find($request->id);

        if ($news == null) {
            return dd($request->id);
        }

        $this->news->where('id', $request->id)->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'content' => $request->news_content,
            'updated_at' => $request->Carbon::now()
        ]);
        return redirect()->route('dashboard.index');
    }

    public function showAll()
    {
        $allNews = $this->news->paginate(5);
        Eventy::addAction('isTinHost', function($news) {

            //Check nếu ngày tạo tin cách hôm nay 2 ngày thì đó là tin host

            $date = date('Y-m-j');
            $hostDate = strtotime ( '-2 day' , strtotime ( $date ) ) ;
            $hostDate = date ( 'Y-m-j' , $hostDate );

            if (strtotime($news->created_at) > strtotime($hostDate)) {
                echo '<p style="color: red">Host host host</p>';
            }
        });
        return view('listnews', compact('allNews'));
    }


    public function test(){

    }

}
